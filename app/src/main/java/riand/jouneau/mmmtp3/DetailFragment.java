package riand.jouneau.mmmtp3;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;

public class DetailFragment extends Fragment {

    WebView mWebView;
    Button mButton;

    public DetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        mWebView = (WebView) view.findViewById(R.id.idWebView);
        String url = ((MainActivity) getActivity()).getUrl();
        TextView mWarningNoSelect = (TextView) view.findViewById(R.id.idWarningNoSelect);
        boolean testUrl = url==null || url.equals("");
        System.out.println("Url : "+url+" & testUrl : "+testUrl);
        if(url==null || url.equals("")){
            mWarningNoSelect.setVisibility(View.VISIBLE);
            mWebView.setVisibility(View.GONE);
        }
        else{
            mWarningNoSelect.setVisibility(View.GONE);
            mWebView.setVisibility(View.VISIBLE);
            mWebView.setWebViewClient(new CustomWebViewClient());
            mWebView.loadUrl(url);
        }

        mButton = (Button) view.findViewById(R.id.idButtonMap);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MapsFragment();
                FragmentManager fragmentManager = getFragmentManager();

                if (((MainActivity) getActivity()).getIsLarge())
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container2, fragment).commit();
                else
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container1, fragment).commit();
                }
        });

        return view;
    }


    private class CustomWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

    }
}

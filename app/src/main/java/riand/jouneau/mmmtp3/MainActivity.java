package riand.jouneau.mmmtp3;

import android.support.v4.app.FragmentManager;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.lang.reflect.GenericArrayType;

public class MainActivity extends AppCompatActivity {

    private String url;
    private boolean isLarge;
    private float lat;
    private float lng;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isLarge = ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) ||
                ((getResources().getConfiguration().screenLayout &
                        Configuration.SCREENLAYOUT_SIZE_MASK) ==
                        Configuration.SCREENLAYOUT_SIZE_XLARGE);

        System.out.println("isLarge : "+isLarge);

        Fragment fragment = new RegionFragment();
        FragmentManager fragmentManager =  getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container1, fragment).commit();
        if(isLarge){
            Fragment fragment2 = new DetailFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container2, fragment2).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean getIsLarge(){
        return isLarge;
    }

    /* Note : both those set and get are here only to provide the url corresponding to the region
    chosen by the user. It only works because both string array have exactly the same position
    between them (each position define a region and the url associated with it). If the goal was to
    create a bigger app, we would have use a class to store those region with their attributes.
     */
    public void setUrl(int position){
        url = getResources().getStringArray(R.array.arrayUrl)[position];
    }

    public String getUrl(){
        return url;
    }

    public void setLatLng(int position){
        String latlng = getResources().getStringArray(R.array.arrayLatLng)[position];
        String[] temp = latlng.split(" ");
        if(temp.length<2) System.err.println("Error : the string array has not the proper length");
        else {
            lat = Float.parseFloat(temp[0]);
            lng = Float.parseFloat(temp[1]);
            System.out.println("Lat : "+lat+" & Long : "+lng);
        }
    }

    public float getLat(){
        return lat;
    }

    public float getLng(){
        return lng;
    }

    public void setName(int position){
        name = getResources().getStringArray(R.array.arrayRegions)[position];
    }

    public String getName(){
        return name;
    }


}

package riand.jouneau.mmmtp3;

import android.app.Activity;
import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class RegionFragment extends Fragment {

    ListView mCustomListView;
    String[] mRegionList;
    ArrayAdapter mListAdapter;

    public RegionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_region, container, false);
        mCustomListView = (ListView) view.findViewById(R.id.customListView);
        initListView();
        return view;
    }

    private void initListView(){
        mRegionList = getResources().getStringArray(R.array.arrayRegions);

        //ArrayAdapter creation
        mListAdapter = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, mRegionList);

        System.out.println("ListAdapter : "+mListAdapter+" -- CustomListView : "+mCustomListView);

        //On attribue à notre listView l'adapter que l'on vient de créer
        mCustomListView.setAdapter(mListAdapter);


        mCustomListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Fragment fragment = new DetailFragment();
                FragmentManager fragmentManager = getFragmentManager();

                if(((MainActivity) getActivity()).getIsLarge())
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container2, fragment).commit();
                else
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame_container1, fragment).commit();
                ((MainActivity) getActivity()).setUrl(position);
                ((MainActivity) getActivity()).setLatLng(position);
                ((MainActivity) getActivity()).setName(position);
            }

        });
    }


}
